target_sources(puissance4 PRIVATE gui.c gui.h)

message("Generating glad...")
set(GLAD_INSTALL 1)
add_subdirectory(${CMAKE_SOURCE_DIR}/thirdparty/glad)
target_include_directories(puissance4 PRIVATE ${GLAD_INCLUDE_DIRS})
link_libraries(${GLAD_LIBRARIES})
target_link_libraries(puissance4 ${GLAD_LIBRARIES})
unset(GLAD_INSTALL)

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

set(GLFW_BUILD_EXAMPLES ON)
set(GLFW_BUILD_TESTS ON)
add_subdirectory(${CMAKE_SOURCE_DIR}/thirdparty/glfw)
target_include_directories(puissance4 PRIVATE ${glfw_INCLUDE_DIRS})
link_libraries(${glfw})
unset(GLFW_BUILD_EXAMPLES)
unset(GLFW_BUILD_TESTS)
target_link_libraries(puissance4 glfw)

