cmake_minimum_required(VERSION 3.5.1)
project(puissance4 C)

set(CMAKE_C_STANDARD 11)

option(WANT_GUI "Adds a GUI to the program." ON)
option(WANT_CLI "Adds a CLI mode to the program." ON)

if (COMMAND cmake_policy)
    cmake_policy (SET CMP0077 NEW)
endif()

message("Detected system: " ${CMAKE_SYSTEM_NAME})

add_library(
    puissance4 STATIC
    main.h main.c dir.c dir.h
)

if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
    message("Including dirent polyfill.")
    target_include_directories(puissance4 PRIVATE ${CMAKE_SOURCE_DIR}/thirdparty/dirent/include)
else()
    message("Posix compliant environment. Polyfill not needed.")
endif()

if(TESTING)
    target_compile_definitions(puissance4 PRIVATE TESTING_PUISSANCE4)
    target_compile_definitions(puissance4 PRIVATE MOCK_PUISSANCE4)
endif()

if (MSVC)
    target_compile_definitions(puissance4 PRIVATE _CRT_SECURE_NO_WARNINGS)
endif()

if(WANT_GUI)
    include("gui.cmake")
    add_executable(gui gui.c gui.h)
    target_link_libraries(gui puissance4)
endif()

if(WANT_CLI)
    add_executable(cli cli.c)
    target_link_libraries(cli puissance4)
endif()