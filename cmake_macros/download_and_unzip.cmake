macro(download_and_unzip url name buildpath incs libs)
    string(TOUPPER ${name} uppername)
    string(REGEX REPLACE "^.+/(.*)\\.zip" "\\1"
            foldername ${url})

    if(NOT EXISTS ${CMAKE_SOURCE_DIR}/thirdparty/${foldername})
        message("Downloading ${name} from Github...")
        execute_process(COMMAND ${CMAKE_COMMAND} -E sleep 1)

        file(
                DOWNLOAD
                ${url}
                ${CMAKE_SOURCE_DIR}/thirdparty/${foldername}.zip
                LOG ${name}dllog
                STATUS ${name}dlstatus
        )

        list(GET ${name}dlstatus 0 ${name}dlstatusval)
        list(GET ${name}dlstatus 1 ${name}dlstatusstr)

        file(WRITE ${CMAKE_SOURCE_DIR}/thirdparty/dl_${name}_log.txt ${${name}dllog})
        message("${name} dowload status:  ${${name}dlstatusval} ${${name}dlstatusstr}")

        if(${name}dlstatusval)
            message(FATAL_ERROR "Couldn't download ${name}. Exiting.")
        else()
            message("Downloaded ${pretty} correctly.")
        endif()

        message("Extracting zip contents...")
        execute_process(
                COMMAND ${CMAKE_COMMAND} -E tar xzf "${foldername}.zip"
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}/thirdparty/
        )
    endif()

    message("Generating subproject...")
    add_subdirectory(${CMAKE_SOURCE_DIR}/thirdparty/${foldername}${buildpath}/)
    message("${${uppername}_LIB_NAME}")

    include_directories(${CMAKE_SOURCE_DIR}/thirdparty/${foldername}/include/)
    include_directories(${${incs}})
    link_libraries(${${libs}})
endmacro()
