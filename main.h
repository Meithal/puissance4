//
// Created by hurin on 26/07/2021.
//

#ifndef PUISSANCE4_MAIN_H
#define PUISSANCE4_MAIN_H
#include <stdio.h>

enum {
    POSSIBLE_SEEDS = 1000
};

#define LARGEUR_DAMIER (10)
#define HAUTEUR_DAMIER (10)

#define LONGUEUR_VICTOIRE (4)
#define NOMBRE_JOUEURS (2)

#define CASE_VIDE '\0'

static char symboles[] = {'X', 'O'};

typedef struct {
    signed int ligne;
    signed int colonne;
} pos_s;

struct vec {
    signed int dvert;
    signed int dhor;
};

static struct vec directions[] = {
        { .dvert = -1, .dhor = 0 }, // Nord
        { .dvert = 1, .dhor = 0 }, // Sud
        { .dvert = 0, .dhor = -1 }, // Ouest
        { .dvert = 0, .dhor = 1 }, // Est
        { .dvert = -1, .dhor = -1 }, // NO
        { .dvert = 1, .dhor = 1 }, // SE
        { .dvert = -1, .dhor = 1 }, // NE
        { .dvert = 1, .dhor = -1 }, // SO
};

#define NOMBRE_DIRECTIONS (sizeof directions / sizeof directions[0])

typedef char dam_t[HAUTEUR_DAMIER][LARGEUR_DAMIER];

struct jr {
    bool humain;
    bool gagnant;
};

typedef int poids;
typedef poids tpds[LARGEUR_DAMIER];

enum poids_colonnes {
    COLONNE_REMPLIE = -1999,
    FAIT_GAGNER_UN_AUTRE = -42,
};

_Static_assert(COLONNE_REMPLIE < FAIT_GAGNER_UN_AUTRE, "Le poids d'une "
                                                       "colonne remplie doit etre inferieur a tous les autres poids de sorte "
                                                       "qu'elle ne soit jamais selectionne par l'IA et provoque un SEGFAULT");


#define RAISONS \
  X(COUP_GAGNANT, "Sienne gagnante") \
  X(CONTRE, "Contre l'ennemi") \
  X(SON_MEILLEUR, "Son meilleur coup") \
  X(SAIT_PERDANT, "Sait qu'il a perdu")

#define X(a, b) a,
enum raisons { RAISONS };
#undef X

#define X(a, b) [a] = b,
static char * raisons[] = {
        RAISONS
};
#undef X
#undef RAISONS

typedef signed int serie_coups[LARGEUR_DAMIER * HAUTEUR_DAMIER + 1];


int puissance_10(long puissance);
char lire(dam_t *dam, pos_s pos);
void jouer(dam_t *dam, pos_s pos, char c);
int prochain_joueur(int jr);
bool dans_limites(pos_s pos);
bool colonne_remplie(dam_t * dam, int colonne);
int poids_est_gagnant(poids poids);
pos_s vers(pos_s origin, struct vec vect);
pos_s case_vide(dam_t * dam, int colonne);
poids poids_case(int jr, dam_t * dam, pos_s orig);
void ponderer(dam_t * dam, int jr, tpds pnd);
int meilleure_colonne(const tpds table_pond);
int choix_ia(
        dam_t * dam,
        int soi,
        int enn,
        tpds pnd_soi,
        tpds pnd_enn,
        int * choix_soi,
        int * choix_enn);
enum raisons raison_ia(tpds * pnd_soi, tpds * pnd_enn, const int * choix_soi, const int * choix_enn);

int sauvegarder(int seed, serie_coups coups_joues, int tour);
bool load_saved_game(char * filename, int * seed, serie_coups coups_joues, int * tour);
bool nom_valide(char nom[FILENAME_MAX]);


#endif //PUISSANCE4_MAIN_H
