//
// Created on 14/03/2019.
//

#include <dirent.h>
#include <stdlib.h>
#include <string.h>

#include "dir.h"


struct directory * claim_directory(const char *path, bool (*comp_function)(char *)) {

    struct directory * dir = malloc(sizeof(struct directory));

    if (dir == NULL) {
        goto end;
    }

    dir->count = 0;

    DIR * dh = opendir(path);

    if (dh == NULL) {
        goto free;
    }

    struct dirent * de;
    while ((errno = 0, de = readdir(dh))) {

        if (errno) {
            perror(strerror(errno));
            goto free;
        }

        if (comp_function(de->d_name)) {

            dir->count += 1;

            void * newdir = realloc(dir, sizeof(*dir) + sizeof(dir->filenames[0]) * dir->count);

            if (newdir == NULL) {
                goto free;
			}

            dir = newdir;

            strcpy(dir->filenames[dir->count - 1], de->d_name);
        }
    }

    if (0) {
        free:
        free(dir);
        dir = NULL;
    }

    closedir(dh);

    end:
    return dir;
}


void unclaim_directory(struct directory * dir) {
    free(dir);
}