#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>

#include "main.h"


/** Fonctions utilitaires */

/**
 * -> 100 pour 2, 1000 pour 3...
 * todo: verifier integer overflow
 **/
int puissance_10(long puissance) {
    int mult;

    mult = 1;

    if (puissance < 0) {
        mult = 0;
    } else while (puissance--) {
        mult *= 10;
    }

    return mult;
}
/** End */


/** Fonctions rendant le code plus lisible */

char lire(dam_t *dam, pos_s pos) {

    return (*dam)[pos.ligne][pos.colonne];
}

void jouer(dam_t *dam, pos_s pos, char c) {

    (*dam)[pos.ligne][pos.colonne] = c;
}

/* -> pointeur sur le prochain joueur � jouer dans la partie */
int prochain_joueur(int jr) {

    return (jr + 1) % NOMBRE_JOUEURS;
}

bool dans_limites(pos_s pos) {
    return (pos.ligne >= 0 && pos.ligne < HAUTEUR_DAMIER
            && pos.colonne >= 0 && pos.colonne < LARGEUR_DAMIER);
}

bool colonne_remplie(dam_t * dam, int colonne) {
    return (*dam)[0][colonne] != CASE_VIDE;
}

int poids_est_gagnant(poids poids) {
    return poids >= puissance_10(LONGUEUR_VICTOIRE - 1);
}

pos_s vers(pos_s origin, struct vec vect) {
    return (pos_s) {
        origin.ligne + vect.dvert,
        origin.colonne + vect.dhor
    };
}
/** End */


/**
 * -> position de la case vide de la colonne donn�e.
 * Appeler cette fonction sur une colonne remplie est une erreur.
 * Verifier que la premiere case soit bien vide avant.
 */
pos_s case_vide(dam_t * dam, int colonne) {
    pos_s cur = (pos_s){0, colonne};
    struct vec sud = { .dvert = 1, .dhor = 0 };

    for ( pos_s cible;
         cible = vers(cur, sud),
                 dans_limites(cible) && (lire(dam, cible) == CASE_VIDE);
         cur = cible)
    {}

    return cur;
}

/**
 * -> poids de la case demand�e.
 */
poids poids_case(int jr, dam_t * dam, pos_s orig) {
    poids long_;
    poids long_cap;
    size_t i_dir;
    size_t i_sens;
    bool vide_trouve;
    char ct;
    poids taille[] = { 1, 1, 1, 1 };
    poids cap[] = { 1, 1, 1, 1 };
    poids poids = 0;

    for (i_dir = 0; i_dir < NOMBRE_DIRECTIONS; i_dir++) {
        long_ = long_cap = 0;
        vide_trouve = false;
        struct vec card = directions[i_dir];
        for (pos_s cur = orig;
             cur = vers(cur, card),
                     dans_limites(cur)
               && ((ct = lire(dam, cur)) == CASE_VIDE || ct == symboles[jr]);
            /**/) {

            if (ct == CASE_VIDE) {
                vide_trouve = true;
            }

            if (!vide_trouve) {
                long_++;
            }

            long_cap++;
        }

        taille[i_dir / 2] += long_;
        cap[i_dir / 2] += long_cap;
    }

    for (i_sens = 0; i_sens < 4; i_sens++) {
        if (cap[i_sens] < LONGUEUR_VICTOIRE) {
            taille[i_sens] = 0;
        }
    }

    for (i_sens = 0; i_sens < 4; i_sens++) {
        poids += puissance_10(taille[i_sens] - 1);
    }

    return poids;
}

/**
 * Poids qui sera per�u par l'IA pour chaque colonne
 * du damier pour un joueur donn�.
 */
void ponderer(dam_t * dam, int jr, tpds pnd) {
    int i;

    for (i = 0; i < LARGEUR_DAMIER; i++) {
        if (colonne_remplie(dam, i)) {
            pnd[i] = COLONNE_REMPLIE;
            continue;
        }

        pnd[i] = poids_case(jr, dam, case_vide(dam, i));
    }
}

/**
 * -> index de la colonne qui a le meilleur poids
 * (au hasard si egalit�)
 */
int meilleure_colonne(const tpds table_pond) {
    int max_count = 0;
    int val_max = 0;
    int index, index_max = 0;

    for (index = 0; index < LARGEUR_DAMIER ; index++) {

        if (table_pond[index] > val_max) {
            val_max = table_pond[index];
            max_count = 1;
            index_max = index;
        }
        else if (table_pond[index] == val_max) {
            max_count++;

            if (rand() < RAND_MAX / (max_count + 1)) { // NOLINT(cert-msc30-c,cert-msc50-cpp)
                val_max = table_pond[index];
                index_max = index;
            }
        }
    }

    return index_max;
}

/**
 * -> un num�ro de colonne
 * todo: plutot que de simuler un damier dans le futur,
 * calculer le nombre de coups necessaires
 * pour avoir un coup gagnant et voir si il est impair
 * (jouable par l'ennemi)
 **/
int choix_ia(
        dam_t * dam,
        int soi,
        int enn,
        tpds pnd_soi,
        tpds pnd_enn,
        int * choix_soi,
        int * choix_enn) {
    long choix_virt;
    dam_t dam_virt;
    tpds pnd_virt;
    int i_col;

    ponderer(dam, soi, pnd_soi);

    *choix_soi = meilleure_colonne(pnd_soi);

    if (!poids_est_gagnant(pnd_soi[*choix_soi]))  {

        ponderer(dam, enn, pnd_enn);

        for (i_col = 0; i_col < LARGEUR_DAMIER; i_col++) {

            if (colonne_remplie(dam, i_col)) continue;

            memcpy(&dam_virt, dam, sizeof(*dam));
            jouer(&dam_virt, case_vide(&dam_virt, i_col), symboles[soi]);
            ponderer(&dam_virt, enn, pnd_virt);
            choix_virt = meilleure_colonne(pnd_virt);

            if (poids_est_gagnant(pnd_virt[choix_virt])) {
                pnd_soi[i_col] = FAIT_GAGNER_UN_AUTRE;
                pnd_enn[i_col] = FAIT_GAGNER_UN_AUTRE;
            }
        }

        *choix_soi = meilleure_colonne(pnd_soi);
        *choix_enn = meilleure_colonne(pnd_enn);
        if (pnd_enn[*choix_enn] > pnd_soi[*choix_soi]) {
            choix_soi = choix_enn;
        }
    }

    return *choix_soi;
}

enum raisons raison_ia(
        tpds * pnd_soi,
        tpds * pnd_enn,
        const int * choix_soi,
        const int * choix_enn) {
    enum raisons raison;

    if (poids_est_gagnant((*pnd_soi)[*choix_soi])) {
        raison = COUP_GAGNANT;
    }
    else {
        if ((*pnd_enn)[*choix_enn] > (*pnd_soi)[*choix_soi]) {
            raison = CONTRE;

        } else {
            raison = SON_MEILLEUR;

            if((*pnd_soi)[*choix_soi] == FAIT_GAGNER_UN_AUTRE) {
                raison = SAIT_PERDANT;
            }
        }
    }

    return raison;
}

/* Sauvegarde la partie dans un fichier */
int sauvegarder(int seed, serie_coups coups_joues, int tour) {
    enum {
        buffer_size = 30
    };
    size_t the_time = (size_t)time(NULL);
    char buf_string[buffer_size];

    if(the_time == -1) return -1;

    sprintf(buf_string, "%s%zu%s", "SAV", the_time, "sav.txt");

    FILE * save = fopen(buf_string, "wb");

    if(save == NULL) return -1;

    fprintf(save, "Seed: %d\tLast turn: %d\r\n", seed, tour);
    for(int i = 0 ; i < tour ; i++) {
        fprintf(save, "%d\r\n", coups_joues[i]);
    }
    fclose(save);

    return 0;
}


/**
 * Load a save file into memory slot pointers
 */
bool load_saved_game(
        char * filename,
        int * seed,
        serie_coups coups_joues,
        int * tour) {
    enum {
        buffsize = 10,
    };
    char fgetsret[buffsize];
    char * rec;
    int coup_joue;
    int tourmax;

    FILE * save = fopen(filename, "rb");

    if (save == NULL) return 0;

    int scanfret = fscanf(save, "Seed: %d\tLast turn: %d", seed, tour);

    if(scanfret != 2) {
        fclose(save);
        return false;
    }

    tourmax = 0;
    while(tourmax++ < *tour) {
        fscanf(save, " ");  /* discard accidental white space from old saves*/
        fgets(fgetsret, buffsize, save);
        coup_joue = (int)strtoul(fgetsret, &rec, 10);

        if (fgetsret == rec) {tourmax--; continue;}

        coups_joues[tourmax-1] = coup_joue;

    }

    fclose(save);

    return true;
}

bool nom_valide(char nom[FILENAME_MAX]) {
    return (strstr(nom, "SAV") == nom);
}