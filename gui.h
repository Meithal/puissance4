//
// Created on 07/04/2019.
//

#ifndef PUISSANCE4_GUI_H
#define PUISSANCE4_GUI_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

struct graphic_window_data {
	bool initialized;
	int monitorCount;
};

void open_windows(struct graphic_window_data*);

static void list_modes(GLFWmonitor* monitor);
static void test_modes(GLFWmonitor* monitor);
static void error_callback(int error, const char* description);

void close_windows();

#endif //PUISSANCE4_GUI_H
