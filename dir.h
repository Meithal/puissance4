//
// Created on 14/03/2019.
//

#ifndef PUISSANCE4_DIR_H
#define PUISSANCE4_DIR_H

#include <stdbool.h>
#include <stdio.h>


struct directory{
    int count;
    char filenames[][FILENAME_MAX];
} * claim_directory(const char * path, bool comp_function(char name[FILENAME_MAX]));


void unclaim_directory(struct directory * dir);


#endif //PUISSANCE4_DIR_H
