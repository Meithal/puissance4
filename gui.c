//
// Created on 07/04/2019.
//
#include <time.h>

#include "gui.h"
#include "main.h"

#define WIDTH 640
#define HEIGHT 480


#define UNSET_INDEX -1
static int last_index_clicked = UNSET_INDEX;
static int play_demanded = 0;

void close_windows() {
	glfwTerminate();
}

static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s (%d)\n", description, error);
}

static void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    (void)window;
	printf("Framebuffer resized to %ix%i\n", width, height);

	glViewport(0, 0, width, height);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    (void)scancode;
    (void)mods;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}


void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    (void)mods;
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    if (action == GLFW_RELEASE) {
        play_demanded = 1;

        if(button == GLFW_MOUSE_BUTTON_LEFT) {
            double hor = xpos / (double )WIDTH * 100.;
            hor = hor * LARGEUR_DAMIER / 100;
            last_index_clicked = (int)hor;
        }
    }
}

float idx_to_glpos(int idx) {
    float chunck = (float )WIDTH / (float )LARGEUR_DAMIER * 2* ((float)idx +3);
    float xpos = chunck/(float )WIDTH;
    return xpos - 1;
}

float idx_to_ypos(int idx) {
    float chunck = (float )HEIGHT / (float )HAUTEUR_DAMIER * 2* ((float)idx +1);
    float xpos = 2 - chunck/(float )HEIGHT;
    return (xpos - 1);
}

void draw_circle(int pos, int height, char color) {
    enum {
        red,
        yellow
    } kind = (color == symboles[0]);
    float xpos = idx_to_glpos(pos);
    float ypos = idx_to_ypos(height);

    glBegin(GL_TRIANGLE_FAN);
        switch (kind) {
            case red:glColor4f(1.0f, 0, 0, 1.f);break;
            case yellow:glColor4f(1.0f, 1.0f, 0, 1.f);break;
            default:;
        }
        glVertex2f(xpos -0.5f -.1f, ypos);
        glVertex2f(xpos -0.5f, ypos+.1f);
        glVertex2f(xpos -0.5f + .1f, ypos+.1f);
    glEnd();
}

void draw(dam_t *dam) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (int i = 0; i < LARGEUR_DAMIER; i++) {
        glBegin(GL_LINES);
        glColor4f(1.0f, 0, 0, 1.f);
        float xpos = idx_to_glpos(i-2);
        glVertex2f(xpos, -1);
        glVertex2f(xpos, 1);
        glEnd();
    }

    for (int ligne = 0; ligne < HAUTEUR_DAMIER; ++ligne) {
        for (int colonne = 0;
             colonne < LARGEUR_DAMIER; ++colonne) {
            if(lire(dam, (pos_s){.ligne=ligne, .colonne=colonne}) == CASE_VIDE) {
                continue;
            }
            draw_circle(colonne, ligne, lire(dam, (pos_s){.ligne=ligne, .colonne=colonne}));
        }
    }

    glFlush();
}

int get_column(struct jr joueurs[2], int ja, dam_t * dam) {
    if (joueurs[ja].humain) {
        //todo
        return 0;
    }
    tpds pnd_soi, pnd_enn;
    int choix_soi, choix_enn;

    if(last_index_clicked != UNSET_INDEX) {
        return last_index_clicked;
    }
    return choix_ia(
            dam, ja, prochain_joueur(ja),
            pnd_soi, pnd_enn, &choix_soi, &choix_enn
    );
}

int logic(dam_t *dam, struct jr joueurs[2], int ja) {
    bool egalite = false;

    int colonne_jouee = get_column(joueurs, ja, dam);
    if (colonne_jouee == -1) {
        return EXIT_SUCCESS;
    }

    pos_s case_jouee = case_vide(dam, colonne_jouee);
    jouer(dam, case_jouee, symboles[ja]);
    if (poids_est_gagnant(poids_case(ja, dam, case_jouee))) {
        joueurs[ja].gagnant = 1;
        return EXIT_SUCCESS;
    }

    egalite = true;

    for (int i = 0; i < LARGEUR_DAMIER; i++) {
        if (!colonne_remplie(dam, i)) {
            egalite = false;
            break;
        }
    }
    if (egalite) {
        return EXIT_SUCCESS;
    }
    return 1;
}

int main(void) {
    GLFWwindow * window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Puissance 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwSetKeyCallback(window, key_callback);
    glfwSetInputMode(window, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        puts("Failed to initialize OpenGL context");
        return -1;
    }
    glViewport(0, 0, WIDTH, HEIGHT);
    glClearColor(0.0f ,1.f ,0.f ,.5f);

    struct jr joueurs[2] = {0};
    dam_t dam = {0};

    time_t the_time = time(NULL);

    if (the_time == -1) {
        puts("Cannot initialize the time.");
        return EXIT_FAILURE;
    }

    int seed = (int) the_time % POSSIBLE_SEEDS;

    srand((unsigned int)seed);
    int ja = rand() % NOMBRE_JOUEURS;  // NOLINT(cert-msc50-cpp,cert-msc30-c)
    bool termine = false;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        /* Render here */
        draw(&dam);
        /* Swap front and back buffers */
        glfwSwapBuffers(window);
        /* Poll for and process events */
        glfwPollEvents();
        if(termine) continue;
        if(!play_demanded) continue;
        if(logic(&dam, joueurs, ja) == EXIT_SUCCESS) {
            termine = true;
        }
        ja = prochain_joueur(ja);
        play_demanded = 0;
        last_index_clicked = UNSET_INDEX;
    }

    glfwTerminate();
    return 0;
}
