#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>
#include <stdbool.h>

#include "dir.h"
#include "main.h"

char demander_lettre(void) {
    char lettre[3];

    fflush(stdout);
    fgets(lettre, 3, stdin);
    (void)sscanf(lettre, "%3s%*[^\n]%*c", lettre);

    return lettre[0];
}

/* Demande d'entrer un nombre entre 0 et INT_MAX,
 * -> la valeur entree convertie en int, -1 si entree invalide */
int demander_nombre_naturel(void) {
    enum{
        buffer_size = 10
    };
    char string_rec[buffer_size];
    int val;

    fflush(stdout);
    fgets(string_rec, buffer_size, stdin);
    val = sscanf(string_rec, "%9s%*[^\n]%*c", string_rec);

    if(val == EOF) {
        val = -1;
    } else {
        val = (int)strtoul(string_rec, NULL, buffer_size);
        if(val < 0) {
            val = -1;
        }
    }

    return val;
}


struct mode_affichage {
    bool numeros: 1;
    bool separation_horizontale: 1;
    char bord_gauche;
    char separateur_vertical;
    char * bord_droit;
    char * coin_gauche;
    char * coin_droit;
    char * contenu;
    char * separateur_horizontal;
};

bool afficher_numeros_ligne(void) {

    size_t i;

    for (i = 1; i <= LARGEUR_DAMIER; i++) {
        printf("  %zd ", i);
    }

    putchar('\n');

    return true;
}

void afficher_separation_horizontale(char * coin_g, char * sep, char * coin_d) {
    size_t i;

    printf("%s", coin_g);
    for (i = 1; i <= LARGEUR_DAMIER; i++) {
        fputs(sep, stdout);
    }

    puts(coin_d);
}

void afficher_damier_commun(dam_t * dam, struct mode_affichage * ma) {

    ma->numeros && afficher_numeros_ligne();
    afficher_separation_horizontale(ma->coin_gauche, ma->separateur_horizontal, ma->coin_droit);

    for (int ligne = 0; ligne < HAUTEUR_DAMIER; ++ligne) {
        putchar(ma->bord_gauche);
        for (int colonne = 0; colonne < LARGEUR_DAMIER; ++colonne) {
            if(colonne && ma->separateur_vertical) {
                putchar(ma->separateur_vertical);
            }
            printf(ma->contenu, (*dam)[ligne][colonne] ? (*dam)[ligne][colonne] : ' ');
        }
        puts(ma->bord_droit);
        if(ma->separation_horizontale || ligne+1 == HAUTEUR_DAMIER) {
            afficher_separation_horizontale(ma->coin_gauche, ma->separateur_horizontal, ma->coin_droit);
        }
    }

    ma->numeros && afficher_numeros_ligne();
    fflush(stdout);
}

void afficher_damier(dam_t * dam) {
    afficher_damier_commun(dam, &(struct mode_affichage) {
        .numeros = true,
        .separation_horizontale = true,
        .bord_gauche = '|',
        .separateur_vertical = '|',
        .bord_droit = "|",
        .coin_gauche = "",
        .coin_droit = "+",
        .contenu = " %c ",
        .separateur_horizontal = "+---"
    });
}

void afficher_damier_chargement(dam_t * dam) {
    afficher_damier_commun(dam, &(struct mode_affichage) {
        .numeros = false,
        .separation_horizontale = false,
        .bord_gauche = '|',
        .separateur_vertical = '\0',
        .bord_droit = "|",
        .coin_gauche = "+",
        .coin_droit = "+",
        .contenu = "%c",
        .separateur_horizontal = "-"
    });
}

void afficher_parties_sauvegardee(struct directory * dir, int pos) {
    int seed;
    serie_coups coups = {-1};
    int tour;
    int ja;
    int deb;

    printf("Partie numero %d :\n", pos + 1);

    if(!load_saved_game(dir->filenames[pos], &seed, coups, &tour)) {
        puts("Problème lors du chargement de la sauvegarde.");
        return;
    }

    srand((unsigned int)seed);

    dam_t dam = {0};

    ja = rand() % NOMBRE_JOUEURS; // NOLINT(cert-msc50-cpp,cert-msc30-c)

    for(deb = 0; deb < tour; deb++) {

        jouer(&dam, case_vide(&dam, coups[deb] - 1), symboles[ja]);

        ja = prochain_joueur(ja);
    }

    afficher_damier_chargement(&dam);
    printf("Joueur en cours: %c\n\n", symboles[ja]);
}

int get_column(struct jr joueurs[2], int ja, dam_t * dam) {
    int colonne_jouee;
    tpds pnd_soi, pnd_enn;
    int choix_soi, choix_enn;

    if (joueurs[ja].humain) {
        printf(
                "Au joueur %d (%c) de jouer (un chiffre entre 1 et %d)\n> ",
                ja + 1,
                symboles[ja],
                LARGEUR_DAMIER
        );
        fflush(stdout);

        for (;;) {
            colonne_jouee = demander_nombre_naturel() - 1;

            if (!dans_limites((pos_s) {0, colonne_jouee})) {
                puts("Chiffre invalide.");
            } else if (colonne_remplie(dam, colonne_jouee)) {
                puts("Colonne remplie, veuillez en choisir une autre.");
            } else {
                break;
            }

            fputs("Veuillez renseigner une autre valeur\n> ", stdout);
        }
    }
    else {
        colonne_jouee = choix_ia(
                dam, ja, prochain_joueur(ja),
                pnd_soi, pnd_enn, &choix_soi, &choix_enn
        );
    }

    printf("Le joueur %d (%c), (%s) joue à la colonne %d%s%s\n",
           ja + 1,
           symboles[ja],
           joueurs[ja].humain ? "humain" : "ordinateur",
           colonne_jouee + 1,
           joueurs[ja].humain ? "" : " - ",
           joueurs[ja].humain ? "" : raisons[
                   raison_ia(&pnd_soi, &pnd_enn, &choix_soi, &choix_enn)
           ]
    );

    return colonne_jouee;
}

/**
 * Asks for a number of players. Play the game.
 * Returns the number of the winner (0 for player 1, 1, for player 2)
 * Returns -1 in case of a draw.
 * */
int play(int seed, serie_coups coups_joues, int tour) {
    printf("Seed: %d\n", seed);

    struct jr joueurs[2] = {0};

    dam_t dam = {0};
    srand((unsigned int)seed);
    int ja = rand() % NOMBRE_JOUEURS;  // NOLINT(cert-msc50-cpp,cert-msc30-c)

    int colonne_jouee;
    bool egalite = false;

    for(int deb = 0; deb < tour; deb++) {

        jouer(&dam, case_vide(&dam, coups_joues[deb] - 1), symboles[ja]);

        ja = prochain_joueur(ja);
    }
    afficher_damier(&dam);

    (void)puts("Combien de joueurs humains participeront? (défaut: 1)");
    (void)puts(" - Une intelligence artificielle prendra la place du/des absent(s)");
    int i = demander_nombre_naturel();
    if(i > NOMBRE_JOUEURS || i == -1) {
        i = NOMBRE_JOUEURS - 1;
    }
    for (int j = 0; j < NOMBRE_JOUEURS; j++) {
        joueurs[j].humain = (j < i);
    }

    for (;;) {

        colonne_jouee = get_column(joueurs, ja, &dam);

        if (colonne_jouee == -1) {
            return EXIT_SUCCESS;
        }

        pos_s case_jouee = case_vide(&dam, colonne_jouee);
        jouer(&dam, case_jouee, symboles[ja]);

        afficher_damier(&dam);

        if (poids_est_gagnant(poids_case(ja, &dam, case_jouee))) {
            joueurs[ja].gagnant = 1;
            break;
        }

        egalite = true;

        for (i = 0; i < LARGEUR_DAMIER; i++) {
            if (!colonne_remplie(&dam, i)) {
                egalite = false;
                break;
            }
        }
        if (egalite) {
            break;
        }

        ja = prochain_joueur(ja);

        puts("\n- - Appuyez sur [Entrée] pour continuer, [q] pour quitter [s] pour sauvegarder & quitter - -");
        switch (demander_lettre()) {
            case 's':
                sauvegarder(seed, coups_joues,
                            tour); /* absence de break volontaire */
            case 'q':
                return -1;
            default:
                break;
        }
    }

    int gagnant = -1;
    if (egalite) {
        puts("Egalité !");
    }
    else {
        for (i = 0; i < NOMBRE_JOUEURS; i++) {
            if (joueurs[i].gagnant) {
                printf("Le joueur %d (%c) a gagné\n",
                       ja + 1,
                       symboles[ja]
                );
                gagnant = i;
            }
        }
    }

    afficher_damier(&dam);
    printf("Seed: %d\n", seed);

    for(;;) {
        puts("\n- - Appuyez sur [q] pour quitter [s] pour sauvegarder & quitter - -");

        switch (demander_lettre()) {
            case 's':
                sauvegarder(seed, coups_joues, tour); /* absence de break volontaire */
            case 'q': {
                return gagnant;
            }
            default:
                continue;
        }
    }
}

/**
 * A game holds a seed, a history of played columns so it
 * can be replayed and resumed after a save.
 * @return
 */
int main(void) {
    int seed = -1;
    serie_coups coups_joues;
    int tour = -1;

    struct directory * dir = claim_directory(".", nom_valide);

    if(dir == NULL) {
        puts("Something went wrong...");
        return EXIT_FAILURE;
    }

    if(dir->count > 0) {
        fputs("Nouvelle partie [Entree](defaut) ou charger partie? [c]", stdout);
        if (demander_lettre() == 'c') {

            for(int i = 0; i < dir->count ; i++) {
                afficher_parties_sauvegardee(dir, i);
            }

            fwprintf(stdout, L"Quelle partie voulez-vous continuer? (ou [Entree])\n> ");
            int c;
            if((c = demander_nombre_naturel()) > 0 && c <= dir->count) {
                load_saved_game(dir->filenames[c - 1], &seed, coups_joues, &tour);
            }
        }
    } else {
        printf("Entrez une seed (0-%d) ou appuyez sur [Entree] pour une seed aléatoire.\n", POSSIBLE_SEEDS - 1);
        seed = demander_nombre_naturel() % POSSIBLE_SEEDS;
        if (seed < 0) {
            time_t the_time = time(NULL);

            if (the_time == -1) {
                puts("Cannot initialize the time");
                return EXIT_FAILURE;
            }

            seed = (int) the_time % POSSIBLE_SEEDS;
        }
    }

    unclaim_directory(dir);

	int status;
    return (status = play(seed, coups_joues, tour)), /*close_windows(),*/ status != -1;
}
